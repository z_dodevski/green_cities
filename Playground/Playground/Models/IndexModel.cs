﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Playground.Models
{
    public class IndexModel
    {
        public List<String> surfaceFileNames { get; set;  }
        public List<String> refinedFileNames { get; set;  }

        public IndexModel()
        {
            this.surfaceFileNames = new List<string>();
            this.refinedFileNames = new List<string>();
        }
    }
}