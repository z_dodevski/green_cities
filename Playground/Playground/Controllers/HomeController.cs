﻿using Playground.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Playground.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            IndexModel model = new IndexModel();

            string surface = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "img/extracts_surface");
            string refinement = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "img/refinement"); 

            DirectoryInfo d = new DirectoryInfo(surface);//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.png"); //Getting Text files
            foreach (FileInfo file in Files)
            {
                model.surfaceFileNames.Add(file.Name);
            }

            d = new DirectoryInfo(refinement);
            Files = d.GetFiles("*.png"); //Getting Text files
            foreach (FileInfo file in Files)
            {
                model.refinedFileNames.Add(file.Name);
            }
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}