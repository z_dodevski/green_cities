﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreenCities.MVC.Models
{
    public class MapModel
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public MapModel(double longitude, double latitude)
        {
            this.Longitude = longitude;
            this.Latitude = latitude;
        }
    }
}