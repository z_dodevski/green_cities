﻿using GreenCities.MVC.Models;
using NReco.ImageGenerator;
using NReco.PhantomJS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GreenCities.MVC.Controllers
{
    public class MapController : Controller
    {
        // GET: Map
        public ActionResult Index(double end = 41.9970, double start = 21.4280)
        {
            double increment = 0.0254;


            MapModel mapModel = new MapModel(start, end);

            start = 21.301; 
            end = 41.9716;
            var phantomJS = new PhantomJS();

            while (end < 42.052630)
            {
                start = 21.301;
                while (start < 21.555)
                {
                    string fileName = Path.Combine(Server.MapPath("~/App_Data/Images"), "Map_" + start.ToString() + "_" + end.ToString() + ".png");
                    string fileNameSurface = Path.Combine(Server.MapPath("~/App_Data/Images"), "Surface_Map_" + start.ToString() + "_" + end.ToString() + ".png");

                                 phantomJS.Run(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "rasterize.js"),
                    new[] { "http://localhost:53421/map/details?end=" + end + "&start=" + start, fileName });

                    phantomJS.Run(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "rasterize.js"),
new[] { "http://localhost:53421/map/surface?end=" + end + "&start=" + start, fileNameSurface });

                    start = start + increment;
                }
                end = end + increment;
            }           
            phantomJS.Abort();
            return View(mapModel);
        }

        // GET: Map/Details/5
        public ActionResult Details(double end = 41.9970, double start = 21.4280)
        {

            MapModel mapModel = new MapModel(start, end);

            return View(mapModel);
        }

        public ActionResult Surface(double end = 41.9970, double start = 21.4280)
        {

            MapModel mapModel = new MapModel(start, end);

            return View(mapModel);
        }

        // GET: Map/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Map/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Map/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Map/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Map/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Map/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public string RenderViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
