﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace ImageProcessing
{
    class Program
    {
        
        static void Main(string[] args)
        {
            int totalPixelCount = 0;
            int greenPixelCount = 0;
            try
            {
                Program pg = new Program();
                Bitmap bmp = null;
                Bitmap overlaybmp = null;
                string sourcePath = "..\\..\\ImagesSource\\";
                string destPath = "..\\..\\ProcessedImages";
                Color googleMapsGreen = Color.FromArgb(255, 198, 231, 184);
                Color baseColor = Color.Green;
                Color overlayColor = Color.Red;
                float percentage = 0.0F;
                


                if (!Directory.Exists(destPath))
                    Directory.CreateDirectory(destPath);

                string[] files = Directory.GetFiles(sourcePath);
                foreach (string filename in files.Where(x => !x.Contains("Surface")).ToList())
                {
                    string[] spliter = filename.Split('\\');
                    overlaybmp = (Bitmap)Image.FromFile(files.Single(x => x.Contains("Surface") && x.Contains(spliter.Last())));
                    bmp = (Bitmap)Image.FromFile(filename);
                    Bitmap procbmp = pg.CalculateColorPercentage(bmp, overlaybmp, googleMapsGreen, out percentage, ref totalPixelCount, ref greenPixelCount);

                    procbmp.Save(destPath + "\\" + percentage.ToString("0.00") + "___" + spliter.Last());
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            float totalPercentage = (greenPixelCount * 100) / (float)totalPixelCount;
            Console.WriteLine(totalPercentage.ToString("0.00"));
            Console.ReadKey();
        }
        public Bitmap CalculateColorPercentage(Bitmap srcBitmap, Bitmap overlayBmp, Color baseColor, out float percentage, ref int totalPixelCount, ref int greenPixelCount)
        {
            int localTotalPixelCount = 0;
            int pixelsMatched = 0;
            Color overlayColor = Color.Red;
            //make an empty bitmap the same size as srcBitmap
            Bitmap newBitmap = new Bitmap(srcBitmap.Width, srcBitmap.Height);
            for (int i = 0; i < srcBitmap.Width; i++)
            {
                for (int j = 0; j < srcBitmap.Height; j++)
                {
                    Color overlayPixel = overlayBmp.GetPixel(i, j);
                    if (IsShade(overlayPixel, overlayColor))
                    {
                        localTotalPixelCount++;
                        totalPixelCount++;
                        Color pixelColor = srcBitmap.GetPixel(i, j);
                        if (IsShade(baseColor, pixelColor))
                        {
                            newBitmap.SetPixel(i, j, pixelColor);
                            greenPixelCount++;
                            pixelsMatched++;
                        }
                        else
                            newBitmap.SetPixel(i, j, Color.Gray);
                    }
                }
            }
            percentage = (pixelsMatched * 100) / (float)localTotalPixelCount;
            return newBitmap;
        }
        public static bool IsShade(Color baseColor, Color pixel)
        {
            int diffR = Math.Abs(baseColor.R - pixel.R);
            int diffG = Math.Abs(baseColor.G - pixel.G);
            int diffB = Math.Abs(baseColor.B - pixel.B);

            int index = (diffR + diffG + diffB) / 3;
            return index < 20;
        }
    }
}
